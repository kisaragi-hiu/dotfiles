# Icons

The hicolor theme is equivalent to a global fallback.

> It is recommended that the icons installed in the hicolor theme look neutral, since it is a fallback theme that will be used in combination with some very different looking themes. But if you don't have any neutral icon, please install whatever icon you have in the hicolor theme so that all applications get at least some icon in all themes. 
> https://specifications.freedesktop.org/icon-theme-spec/icon-theme-spec-latest.html
