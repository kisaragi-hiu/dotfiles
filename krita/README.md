# Krita config

I sometimes use Krita installed with Flatpak, and sometimes use Krita from the system's repositories. The former looks for configuration in `~/.var/app/org.kde.krita/{cache,config,data}` (each corresponding to `.cache`, `.config`, and `.local/share`), while the latter looks in `~/.config` and `~/.local/share`.

To make the two share the same config, I've placed the actual config in the flatpak-style folder structure, adding .config and .local/share as symlinks to the corresponding folders in this repository. This actually works: stow knows how to deal with this.

This can be used for any app that may store configuration in different places depending on how it's installed.
