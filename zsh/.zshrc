#!/bin/zsh
# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

if false; then
    source "$HOME/.zsh/timing.zsh"
else
    # dummy functions
    function k-time-start () {
    }
    function k-time-end () {
    }
fi

k-time-start startup

source "$HOME/.zsh/helpers.zsh"

k-time-start pkgs
source "$HOME/.zsh/packages.zsh"
k-time-end pkgs
source "$HOME/.zsh/settings.zsh"
source "$HOME/.zsh/keybinds.zsh"
source "$HOME/.zsh/commands.zsh"
source! "$HOME/.env.local"

if __has_command zoxide; then
    _evalcache zoxide init zsh --cmd cd
fi

k-status() {
    emulate -L zsh
    if git rev-parse 2>/dev/null; then
        echo
        git status -s 2>/dev/null
    fi
    if [ -d ".svn" ]; then
        echo "SVN status:"
        svn status
    fi
}
add-zsh-hook chpwd k-status

# source! "$HOME/.p10k.zsh"
k-time-end startup
