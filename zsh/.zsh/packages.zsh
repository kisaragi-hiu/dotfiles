# Initialize zplug
k-time-start zinit
if [[ -f ~/.zinit/bin/zinit.zsh ]]; then
    source ~/.zinit/bin/zinit.zsh
elif __ask "zinit not found. Install it?"; then
    git clone --depth=1 https://github.com/zdharma-continuum/zinit.git ~/.zinit/bin
    source ~/.zinit/bin/zinit.zsh
else
    # dummy function
    function zinit {
        return
    }
fi
k-time-end zinit

k-time-start libraries
# Libraries
zinit for \
    depth"1" light-mode wait lucid \
    "mafredri/zsh-async" \
    "mroth/evalcache"
k-time-end libraries

k-time-start prompt
# Theme
# Only load the prompt when in interactive use
if [[ -o interactive ]] && [[ "$TERM" != dumb ]]; then
    zinit ice depth=1 pick=async.zsh src=pure.zsh
    zinit light "sindresorhus/pure"
    # - %F{242}: start using color 242; %f: stop (else it'll bleed into later
    #   text)
    #   242 is a gray color, used also by Pure in its colors for eg. git branches
    # - %D{...}: like elisp (format-time-string "...")
    # - The [] is literal
    RPROMPT='%F{242}[%D{%FT%T%z}]%f'
    # zinit ice depth=1
    # zinit light romkatv/powerlevel10k
    # export GITSTATUS_CACHE_DIR=${XDG_STATE_HOME:-$HOME/.local/state}/gitstatus
fi
k-time-end prompt

k-time-start behavior
# Shell behavior
# zinit ice blockf atpull='zinit creinstall -q .'
zinit for \
    depth=1 light-mode \
    "zsh-users/zsh-completions" \
    "zsh-users/zsh-autosuggestions"
if command -v fzf >/dev/null; then
    # zinit ice depth=1 wait lucid
    zinit ice depth=1
    zinit light "Aloxaf/fzf-tab" # tab complete with fzf
fi
zinit for \
    depth=1 light-mode wait lucid \
    "hlissner/zsh-autopair" \
    "zsh-users/zsh-history-substring-search"
k-time-end behavior

# Apps
k-time-start apps
zinit ice as=program pick=emojify wait lucid
zinit light "mrowa44/emojify"
zinit ice as=program pick=bk wait lucid
zinit light "kisaragi-hiu/bk"
zinit ice from=gitlab as=program pick="project-*" wait lucid
zinit light "kisaragi-hiu/experiment.project-metadata-format"
k-time-end apps

function __zsh-notify-setup {
    zstyle ':notify:*' error-title "Command failed (#{time_elapsed})"
    zstyle ':notify:*' success-title "Command finished (#{time_elapsed})"
    # Why would a notification default to never expire...?
    zstyle ':notify:*' expire-time 5000
    # Put command in message and use "zsh" as title
    # Instead of using command as title
    zstyle ':notify:*' app-name "zsh"
}

k-time-start zsh_notify
if [[ -o interactive ]] && command -v xdotool >/dev/null; then
    zinit ice wait lucid atload=__zsh-notify-setup
    zinit light "marzocchi/zsh-notify"
fi
k-time-end zsh_notify

k-time-start sourcing
source! /usr/share/doc/pkgfile/command-not-found.zsh
source! /usr/share/fzf/completion.zsh
k-time-end sourcing

k-time-start fsyh
# FIXME: Deno's path (when installed via linuxbrew) might not be added yet
# PATH setup should happen BEFORE packages. 
if [ -f "/home/linuxbrew/.linuxbrew/bin/brew" ]; then
    _evalcache /home/linuxbrew/.linuxbrew/bin/brew shellenv
fi
if __has_command deno; then
    if [ ! -f ~/.zinit/completions/_deno ]; then
        deno completions zsh > ~/.zinit/completions/_deno
    fi
fi
zinit ice depth=1 wait lucid atload="zicompinit; zicdreplay" blockf
zinit light "zdharma-continuum/fast-syntax-highlighting"
# zinit light "zsh-users/zsh-syntax-highlighting"
k-time-end fsyh
