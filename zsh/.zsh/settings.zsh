# * zsh modules
zmodload zsh/mathfunc
zmodload zsh/complist

# * completion
setopt menucomplete

# https://github.com/xero/dotfiles/blob/master/zsh/.zsh/autocompletion.zsh
zstyle ':completion:*' auto-description 'specify: %d'
zstyle ':completion:*' completer _expand _complete _correct _approximate
zstyle ':completion:*' format 'Completing %d'
zstyle ':completion:*' group-name ''
zstyle ':completion:*' menu select=2 eval "$(dircolors -b)"
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: hit TAB for more, or the character to insert%s
zstyle ':completion:*' matcher-list '' 'm:{a-z}={A-Z}' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=* l:|=*'
zstyle ':completion:*' menu select
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' use-compctl false
zstyle ':completion:*' verbose true
zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#)*=0=01;31'
zstyle ':completion:*:kill:*' command 'ps -u $USER -o pid,%cpu,tty,cputime,cmd'

# * history
HISTFILE="$HOME/.history"
SAVEHIST=1000000
HISTSIZE=1000000

setopt append_history # required for share_history
setopt share_history
setopt histignorealldups # zsh builtin de-dupe
setopt histignorespace # don't save lines starting with space

# * aliases

function wrap {
    # create a wrapping alias (only when both commands exist)
    local wrapper="$1"
    local cmd="$2"
    if command -v "$wrapper" >/dev/null && command -v "$cmd" >/dev/null; then
        alias "$cmd"="$wrapper $cmd"
    fi
}

# rlwrap stuff
if command -v rlwrap >/dev/null; then
    wrap rlwrap sbcl
    wrap rlwrap guile
    wrap rlwrap clisp
fi

if command -v rga >/dev/null; then
    alias rg=rga
fi

# Aliases for all environments
if command -v lsd >/dev/null; then
    alias ls='lsd -a'
else
    alias ls='ls -a --color=auto'
fi
alias pgrep='pgrep -a' # show invoked command line

alias vim='echo using nvim...; sleep 0.1; nvim'
alias units='units -v'

if [[ -z "$ANDROID_DATA" ]]; then
    # Aliases that shouldn't be in Termux
    alias frog="raco frog"
    alias open='xdg-open'
    alias paste='xclip -o -sel clip'
    alias pollen="raco pollen"
    alias racket-slideshow="/usr/bin/slideshow"
    # if [[ "$(hostname)" == "MF-PC" ]]; then
    #     alias utau="env WINEPREFIX=$HOME/.wineprefix/UTAU wine 'C:/Program Files (x86)/UTAU/utau.exe'"
    # elif [[ "$(hostname)" == "MF-manjaro" ]]; then
    #     alias utau="env WINEPREFIX=$HOME/.wineprefix/UTAU wine 'C:/Program Files/UTAU/utau.exe'"
    # fi
else
    # Work around https://github.com/termux/termux-packages/issues/20717
    # "Closing since workaround has been found" what??
    export GYP_DEFINES="android_ndk_path=''"

    # Termux specific aliases
    alias open='termux-open'
    alias paste='termux-clipboard-get'
    alias emacsclient="emacsclient --socket-name /data/data/com.termux/files/usr/var/run/emacs*/server"
fi

if command -v nvim >/dev/null; then
    export EDITOR=nvim
    export VISUAL=nvim
fi

if [[ $INSIDE_EMACS == vterm ]]; then
    export EDITOR=emacsclient
    export VISUAL=emacsclient
    if [[ -f "/usr/share/emacs/etc/vterm/emacs-vterm-zsh.sh" ]]; then
        # OpenSUSE ships vterm.el in its site-lisp and puts the integration in a
        # different place.
        source! /usr/share/emacs/etc/vterm/emacs-vterm-zsh.sh
    elif [[ -n "$EMACS_VTERM_PATH" ]]; then
        source! "$EMACS_VTERM_PATH"/etc/emacs-vterm-zsh.sh
    fi
fi

# * path

setopt autocd autopushd pushdignoredups

k-time-start path
path=("$HOME/bin" "$HOME/.local/bin" $path)

path=("$HOME/.nix-profile/bin" $path)
path=("$HOME/.cargo/bin" $path)
path=("$HOME/kde/src/kdesrc-build" $path)
path=("$HOME/.config/emacs/bin/" $path)
path=("$HOME/.local/share/npm-global/bin" $path)
path=("$HOME/flutter/bin" $path)

if [ -f "/home/linuxbrew/.linuxbrew/bin/brew" ]; then
    _evalcache /home/linuxbrew/.linuxbrew/bin/brew shellenv
fi

if [ -d "$GOPATH/bin" ]; then
    export PATH="$GOPATH/bin:$PATH"
fi
if [ -d "$HOME/.flashlight/bin" ]; then
    # flashlight
    export PATH="$HOME/.flashlight/bin:$PATH"
fi
if [ -d "$HOME/.bun/bin" ]; then
    export BUN_INSTALL="$HOME/.bun"
    export PATH="$BUN_INSTALL/bin:$PATH"
    fpath+="$BUN_INSTALL"
fi
if [ -d "$HOME/.local/share/pnpm" ]; then
    export PNPM_HOME="$HOME/.local/share/pnpm"
    export PATH="$PNPM_HOME:$PATH"
fi

racket_bin=$(find "$HOME/.local/share/racket" -type d -name bin -print -quit 2>/dev/null)
if [[ "$racket_bin" != "" ]]; then
    path=($racket_bin $path)
fi

if command -v rbenv >/dev/null; then
    _evalcache rbenv init - --no-rehash zsh
elif command -v gem >/dev/null; then
    if [[ -d "$HOME"/.gem ]]; then
    ruby_version=$(ruby --version | cut -d p -f 1 | cut -d " " -f 2)
    path=("$HOME"/.local/share/gem/ruby/"$ruby_version"/bin $path)
    fi
fi
k-time-end path

# * appearance
# ** dircolors
# Dircolors Solarized <https://github.com/seebi/dircolors-solarized>
# generated output from running `dircolors solarized/dircolors.256dark`
# save it here so I don't have to add an eval to my shell init

LS_COLORS='no=00;38;5;244:rs=0:di=00;38;5;33:ln=00;38;5;37:mh=00:pi=48;5;230;38;5;136;01:so=48;5;230;38;5;136;01:do=48;5;230;38;5;136;01:bd=48;5;230;38;5;244;01:cd=48;5;230;38;5;244;01:or=48;5;235;38;5;160:su=48;5;160;38;5;230:sg=48;5;136;38;5;230:ca=30;41:tw=48;5;64;38;5;230:ow=48;5;235;38;5;33:st=48;5;33;38;5;230:ex=00;38;5;64:*.tar=00;38;5;61:*.tgz=00;38;5;61:*.arj=00;38;5;61:*.taz=00;38;5;61:*.lzh=00;38;5;61:*.lzma=00;38;5;61:*.tlz=00;38;5;61:*.txz=00;38;5;61:*.zip=00;38;5;61:*.z=00;38;5;61:*.Z=00;38;5;61:*.dz=00;38;5;61:*.gz=00;38;5;61:*.lz=00;38;5;61:*.xz=00;38;5;61:*.bz2=00;38;5;61:*.bz=00;38;5;61:*.tbz=00;38;5;61:*.tbz2=00;38;5;61:*.tz=00;38;5;61:*.deb=00;38;5;61:*.rpm=00;38;5;61:*.jar=00;38;5;61:*.rar=00;38;5;61:*.ace=00;38;5;61:*.zoo=00;38;5;61:*.cpio=00;38;5;61:*.7z=00;38;5;61:*.rz=00;38;5;61:*.apk=00;38;5;61:*.gem=00;38;5;61:*.jpg=00;38;5;136:*.JPG=00;38;5;136:*.jpeg=00;38;5;136:*.gif=00;38;5;136:*.bmp=00;38;5;136:*.pbm=00;38;5;136:*.pgm=00;38;5;136:*.ppm=00;38;5;136:*.tga=00;38;5;136:*.xbm=00;38;5;136:*.xpm=00;38;5;136:*.tif=00;38;5;136:*.tiff=00;38;5;136:*.png=00;38;5;136:*.PNG=00;38;5;136:*.svg=00;38;5;136:*.svgz=00;38;5;136:*.mng=00;38;5;136:*.pcx=00;38;5;136:*.dl=00;38;5;136:*.xcf=00;38;5;136:*.xwd=00;38;5;136:*.yuv=00;38;5;136:*.cgm=00;38;5;136:*.emf=00;38;5;136:*.eps=00;38;5;136:*.CR2=00;38;5;136:*.ico=00;38;5;136:*.tex=00;38;5;245:*.rdf=00;38;5;245:*.owl=00;38;5;245:*.n3=00;38;5;245:*.ttl=00;38;5;245:*.nt=00;38;5;245:*.torrent=00;38;5;245:*.xml=00;38;5;245:*Makefile=00;38;5;245:*Rakefile=00;38;5;245:*Dockerfile=00;38;5;245:*build.xml=00;38;5;245:*rc=00;38;5;245:*1=00;38;5;245:*.nfo=00;38;5;245:*README=00;38;5;245:*README.txt=00;38;5;245:*readme.txt=00;38;5;245:*.md=00;38;5;245:*README.markdown=00;38;5;245:*.ini=00;38;5;245:*.yml=00;38;5;245:*.cfg=00;38;5;245:*.conf=00;38;5;245:*.h=00;38;5;245:*.hpp=00;38;5;245:*.c=00;38;5;245:*.cpp=00;38;5;245:*.cxx=00;38;5;245:*.cc=00;38;5;245:*.objc=00;38;5;245:*.sqlite=00;38;5;245:*.go=00;38;5;245:*.sql=00;38;5;245:*.csv=00;38;5;245:*.log=00;38;5;240:*.bak=00;38;5;240:*.aux=00;38;5;240:*.lof=00;38;5;240:*.lol=00;38;5;240:*.lot=00;38;5;240:*.out=00;38;5;240:*.toc=00;38;5;240:*.bbl=00;38;5;240:*.blg=00;38;5;240:*~=00;38;5;240:*#=00;38;5;240:*.part=00;38;5;240:*.incomplete=00;38;5;240:*.swp=00;38;5;240:*.tmp=00;38;5;240:*.temp=00;38;5;240:*.o=00;38;5;240:*.pyc=00;38;5;240:*.class=00;38;5;240:*.cache=00;38;5;240:*.aac=00;38;5;166:*.au=00;38;5;166:*.flac=00;38;5;166:*.mid=00;38;5;166:*.midi=00;38;5;166:*.mka=00;38;5;166:*.mp3=00;38;5;166:*.mpc=00;38;5;166:*.ogg=00;38;5;166:*.opus=00;38;5;166:*.ra=00;38;5;166:*.wav=00;38;5;166:*.m4a=00;38;5;166:*.axa=00;38;5;166:*.oga=00;38;5;166:*.spx=00;38;5;166:*.xspf=00;38;5;166:*.mov=00;38;5;166:*.MOV=00;38;5;166:*.mpg=00;38;5;166:*.mpeg=00;38;5;166:*.m2v=00;38;5;166:*.mkv=00;38;5;166:*.ogm=00;38;5;166:*.mp4=00;38;5;166:*.m4v=00;38;5;166:*.mp4v=00;38;5;166:*.vob=00;38;5;166:*.qt=00;38;5;166:*.nuv=00;38;5;166:*.wmv=00;38;5;166:*.asf=00;38;5;166:*.rm=00;38;5;166:*.rmvb=00;38;5;166:*.flc=00;38;5;166:*.avi=00;38;5;166:*.fli=00;38;5;166:*.flv=00;38;5;166:*.gl=00;38;5;166:*.m2ts=00;38;5;166:*.divx=00;38;5;166:*.webm=00;38;5;166:*.axv=00;38;5;166:*.anx=00;38;5;166:*.ogv=00;38;5;166:*.ogx=00;38;5;166:'
export LS_COLORS

# ** term
# Enable 24bit colors on Termux
# This needs to be done until it's widespread enough and terminfo
# includes it.

# 24bit colors allows Emacs themes to not look horrible.
if command -v termux-open > /dev/null; then
    # See: https://stackoverflow.com/questions/14672875/true-color-24-bit-in-terminal-emacs and https://www.reddit.com/r/termux/comments/hi1en2/truecolor_emacs_24bit_on_termux_console/
    tic -x - <<'HERE'
# Use colon separators.
xterm-24bit|xterm with 24-bit direct color mode,
  use=xterm-256color,
  setb24=\E[48:2:%p1%{65536}%/%d:%p1%{256}%/%{255}%&%d:%p1%{255}%&%dm,
  setf24=\E[38:2:%p1%{65536}%/%d:%p1%{256}%/%{255}%&%d:%p1%{255}%&%dm,
# Use semicolon separators.
xterm-24bits|xterm with 24-bit direct color mode,
  use=xterm-256color,
  setb24=\E[48;2;%p1%{65536}%/%d;%p1%{256}%/%{255}%&%d;%p1%{255}%&%dm,
  setf24=\E[38;2;%p1%{65536}%/%d;%p1%{256}%/%{255}%&%d;%p1%{255}%&%dm,
HERE
    export TERM=xterm-24bits
fi

# * misc variables
XDG_DESKTOP_DIR="$HOME"/デスクトップ
XDG_DOWNLOAD_DIR="$HOME"/ダウンロード
XDG_TEMPLATES_DIR="$HOME"/テンプレート
XDG_PUBLICSHARE_DIR="$HOME"/公開
XDG_DOCUMENTS_DIR="$HOME"/ドキュメント
XDG_MUSIC_DIR="$HOME"/音楽
XDG_PICTURES_DIR="$HOME"/画像
XDG_VIDEOS_DIR="$HOME"/ビデオ
DOTFILES_DIR="$HOME"/.dotfiles/
FZF_DEFAULT_COMMAND=find
