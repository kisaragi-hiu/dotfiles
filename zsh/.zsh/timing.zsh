#!/usr/bin/env zsh

function k-time-start () {
    local id="$1"
    local prompt="${2:-$1}"
    # dynamic global variable names
    # ugh
    builtin typeset -g k_time_"$id"_prompt="$prompt"
    # Slightly faster than using `date`.
    # See zshexpn - Parameter Expansion Flags,
    # which refers to zshmisc - EXPANSION OF PROMPT SEQUENCES.
    # In particular, see zshmisc - Date and time.
    # %D{<...>} gives the same result as date +<...>.
    builtin typeset -g k_time_"$id"_start=${(%):-%D{%s%N}}
}
# k-time-end <id>
# End the timer identified by `id`.
function k-time-end () {
    local id="$1"
    local k_time_end=${(%):-%D{%s%N}}
    local promptvar=k_time_"$id"_prompt
    local startvar=k_time_"$id"_start
    printf "${(P)promptvar}: %.3fs\n" $((( k_time_end - $startvar ) / 1000000000.0))
    unset $promptvar $startvar
}
