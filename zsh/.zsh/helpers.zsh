function __has_command {
    command -v $@ &>/dev/null
}

function __ask {
    while true; do
        printf "%s [y/n] " "$1"
        read yn
        case $yn in
            y*) return 0 ;;
            n*) return 1 ;;
            *)
                echo "Not y/n, assuming no"
                return 1
                ;;
        esac
    done
}

# source $1 if it exists
function source! {
    [[ -f "$1" ]] && source "$1"
}

function add-advice! {
    local usage='Usage: add-advice! how target func
Augment the definition of TARGET in some way.
HOW can be one of:
 --after      ORIG_TARGET "$@"; FUNC "$@"
 --before     FUNC "$@"; ORIG_TARGET "$@"'
    local how="$1"
    local target="$2"
    local func="$3"
    local err=0
    () {
        # The "(U)" and "(P)" are "parameter expansion flags".
        for v in target how func; do
            [[ $err == 0 ]] && [[ -z "${(P)v}" ]] \
                && err="Not enough arguments provided"
        done
        case ${(L)how} in
            (--after|--before);;
            (*) err="HOW should be one of --after or --before, but is '${(L)how}' instead";;
        esac
        for v in target func; do
            [[ $err == 0 ]] && ! command -v "${(P)v}" >/dev/null \
                && err="'${(P)v}' is not a command"
        done
    }
    if [[ "$err" != 0 ]]; then
        [[ "$err" != 1 ]] && echo "$err\n"
        echo "$usage"
        return 1
    fi
    local orig_storage="_${target}_orig$((rand48()))"
    # Prevent collision
    while whence "$orig_storage" >/dev/null
    do
          orig_storage="_${target}_orig$((rand48()))"
    done
    case ${(L)how} in
        (--after)
            functions -c "$target" "$orig_storage"
            eval "function \"$target\" {
                \"$orig_storage\" \"\$@\"
                \"$func\" \"\$@\"
            }"
        ;;
        (--before)
            functions -c "$target" "$orig_storage"
            eval "function \"$target\" {
                \"$func\" \"\$@\"
                \"$orig_storage\" \"\$@\"
            }"
        ;;
    esac
}
