source! /usr/share/fzf/key-bindings.zsh

bindkey -v

if command -v atuin >/dev/null; then
    _evalcache atuin init zsh --disable-up-arrow
    bindkey '^[[A' history-substring-search-up
    bindkey '^[[B' history-substring-search-down
    bindkey -M vicmd '?' _atuin_up_search_widget
    bindkey -M vicmd '/' _atuin_up_search_widget
else
    # bindkey '^r' fzf-history-widget
    bindkey '^[[A' history-substring-search-up
    bindkey '^[[B' history-substring-search-down
    bindkey '^[OA' history-substring-search-up
    bindkey '^[OB' history-substring-search-down
    bindkey -M vicmd '?' history-incremental-search-backward
    bindkey -M vicmd '/' history-incremental-search-forward
fi

export KEYTIMEOUT=1 # 1/100 of a second
