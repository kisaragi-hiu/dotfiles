# Various commands.

function cdf () {
  local d=$(env FZF_DEFAULT_COMMAND="find -type d" fzf)
  cd "$d"
}
function cdg() {
    cd $(git rev-parse --show-toplevel) && cdf
}
function openf () {
  local f=$(env FZF_DEFAULT_COMMAND="find -type f" fzf)
  open "$f"
}

function serve () {
    # serve path [port]
    local old_dir="$(pwd)"
    if test -n "$1"; then
        local new_dir="$1"
    else
        local new_dir="$(pwd)"
    fi
    if test -n "$2"; then
        local port="$2"
    else
        local port=8080
    fi
    cd "$new_dir"
    python -m http.server $port
    cd "$old_dir"
}

function launch () {
    nohup $@ >/dev/null 2>/dev/null &!
}

# cd to git repo root
# http://blog.sushi.money/entry/20100211/1265879271
function u () {
    cd ./$(git rev-parse --show-cdup)
    if [ $# = 1 ]; then
        cd $1
    fi
}

function nvm {
    # Lazy loading!
    if [[ -f "/usr/share/nvm/nvm.sh" ]]; then
        source "/usr/share/nvm/nvm.sh"
        export NVM_DIR="$HOME/.nvm"
        mkdir -p "$NVM_DIR"
        unset NPM_CONFIG_PREFIX
        nvm "$@"
        echo "[kisaragi-hiu] Please note that NPM_CONFIG_PREFIX has been unset for this instance of zsh."
    else
        echo "nvm is not installed"
    fi
}
