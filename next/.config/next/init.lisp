;;; Next Browser Config

(in-package :next)

'((#s(key-chord :key-code nil :key-string "0" :modifiers nil)
   #s(key-chord :key-code nil :key-string "5" :modifiers nil)
   #s(key-chord :key-code nil :key-string "x" :modifiers ("c")))
  delete-window
  (#s(key-chord :key-code nil :key-string "5" :modifiers nil)
   #s(key-chord :key-code nil :key-string "x" :modifiers ("c")))
  "prefix"
  (#s(key-chord :key-code nil :key-string "2" :modifiers nil)
   #s(key-chord :key-code nil :key-string "5" :modifiers nil)
   #s(key-chord :key-code nil :key-string "x" :modifiers ("c")))
  make-window
  (#s(key-chord :key-code nil :key-string "x" :modifiers ("m")))
  execute-extended-command
  (#s(key-chord :key-code nil :key-string "s" :modifiers nil)
   #s(key-chord :key-code nil :key-string "h" :modifiers ("c")))
  start-swank
  (#s(key-chord :key-code nil :key-string "o" :modifiers ("c")))
  load-file
  (#s(key-chord :key-code nil :key-string "c" :modifiers nil)
   #s(key-chord :key-code nil :key-string "h" :modifiers ("c")))
  command-inspect
  (#s(key-chord :key-code nil :key-string "h" :modifiers ("c")))
  "prefix"
  (#s(key-chord :key-code nil :key-string "v" :modifiers nil)
   #s(key-chord :key-code nil :key-string "h" :modifiers ("c")))
  variable-inspect
  (#s(key-chord :key-code nil :key-string "w" :modifiers nil)
   #s(key-chord :key-code nil :key-string "x" :modifiers ("c")))
  delete-active-buffer
  (#s(key-chord :key-code nil :key-string "u" :modifiers nil)
   #s(key-chord :key-code nil :key-string "m" :modifiers ("c")))
  bookmark-url
  (#s(key-chord :key-code nil :key-string "t" :modifiers ("c")))
  make-visible-new-buffer
  (#s(key-chord :key-code nil :key-string "m" :modifiers ("c")))
  "prefix"
  (#s(key-chord :key-code nil :key-string "k" :modifiers nil)
   #s(key-chord :key-code nil :key-string "m" :modifiers ("c")))
  bookmark-delete
  (#s(key-chord :key-code nil :key-string "l" :modifiers ("m")))
  set-url-new-buffer
  (#s(key-chord :key-code nil :key-string "k" :modifiers nil)
   #s(key-chord :key-code nil :key-string "x" :modifiers ("c")))
  delete-buffer
  (#s(key-chord :key-code nil :key-string "b" :modifiers nil)
   #s(key-chord :key-code nil :key-string "x" :modifiers ("c")))
  switch-buffer
  (#s(key-chord :key-code nil :key-string "]" :modifiers ("c")))
  switch-buffer-next
  (#s(key-chord :key-code nil :key-string "[" :modifiers ("c")))
  switch-buffer-previous
  nil
  "prefix"
  (#s(key-chord :key-code nil :key-string "x" :modifiers ("c")))
  "prefix"
  (#s(key-chord :key-code nil :key-string "c" :modifiers ("c"))
   #s(key-chord :key-code nil :key-string "x" :modifiers ("c")))
  kill)
