# kisaragi's dotfiles

Using GNU Stow.

Example:

```sh
stow vim
```

This will symlink vim (neovim) config into the right place.

Folders starting with `@` represent user config for different devices.

Folders starting with `%` should not be installed with Stow.
