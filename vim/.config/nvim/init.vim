" == init settings ==
set hidden

" == notes ==
" set scrollbind: binds two views together. useful for eg. translation

" == plugins ==
call plug#begin('~/.vim/plugged')

" Behavior
Plug 'sgur/vim-editorconfig'
Plug 'tpope/vim-sensible'
Plug 'EinfachToll/DidYouMean'
Plug 'tpope/vim-repeat'
Plug 'ConradIrwin/vim-bracketed-paste'
Plug 'junegunn/fzf.vim'

" UI
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'jeffkreeftmeijer/vim-numbertoggle' " relativenumber off where it makes no sense

" Theme
Plug 'joshdick/onedark.vim'
Plug 'luochen1990/rainbow'
let g:rainbow_active = 1

"Editing
" TODO: install another completion framework
" Plug 'bhurlow/vim-parinfer'
" Plug 'jiangmiao/auto-pairs'
Plug 'mbbill/undotree'

Plug 'markonm/traces.vim'

"Motions, Objects
Plug 'tpope/vim-surround' "object: s. ex: c s ( '
Plug 'tpope/vim-commentary' "verb: gc, gcc

"Easymotion
Plug 'easymotion/vim-easymotion' " <leader><leader>[motion]

"vim-textobj-user based
Plug 'kana/vim-textobj-user'
Plug 'kana/vim-textobj-line' "object: al, il -> line

Plug 'guns/vim-sexp' "object: {a,i}{fFse} for forms, toplevel forms, strings, and elements (s-exp)
" (): nearest bracket pair
" <M-{b,w}>: element-wise 'b'/'w' motion
" {g,}<M-e>: element-wise 'ge'/'e' motion (b/w but to the end of word)
" [[, ]]: adjacent toplevel element
" [e , ]e: select adjacent element
" ==: indent form
" =-: indent toplevel form
" more at github.com/guns/vim-sexp

"filetypes / languages
" Plug 'sheerun/vim-polyglot' "lang pack
Plug 'neovim/nvim-lspconfig'
Plug 'dag/vim-fish'
Plug 'tpope/vim-speeddating' | Plug 'jceb/vim-orgmode'
Plug 'Firef0x/PKGBUILD.vim', { 'for': 'PKGBUILD' }
Plug 'plasticboy/vim-markdown', { 'for': 'markdown' }
Plug 'godlygeek/tabular'
Plug 'kovetskiy/sxhkd-vim'
Plug 'vim-scripts/gnuplot-syntax-highlighting'

"apps
Plug 'junegunn/goyo.vim'
Plug 'junegunn/limelight.vim'
Plug 'https://gitlab.com/HiPhish/awk-ward.nvim'
" :term shortcuts
Plug 'DanySpin97/ttab.vim'
" <C-b>{n,p,c,x}: tab: next, previous, new buffer, close current
" <C-b>{t,|,-}: term: new in new tab, new in :vs, new in :split
" <C-b>{0..9}: move to nth tab
let g:ttab_prefix = '<C-b>'

call plug#end()

" == function definitions ==
" http://vim.wikia.com/wiki/Different_syntax_highlighting_within_regions_of_a_file
function! TextEnableCodeSnip(filetype,start,end,textSnipHl) abort
  let ft=toupper(a:filetype)
  let group='textGroup'.ft
  if exists('b:current_syntax')
    let s:current_syntax=b:current_syntax
    " Remove current syntax definition, as some syntax files (e.g. cpp.vim)
    " do nothing if b:current_syntax is defined.
    unlet b:current_syntax
  endif
  execute 'syntax include @'.group.' syntax/'.a:filetype.'.vim'
  try
    execute 'syntax include @'.group.' after/syntax/'.a:filetype.'.vim'
  catch
  endtry
  if exists('s:current_syntax')
    let b:current_syntax=s:current_syntax
  else
    unlet b:current_syntax
  endif
  execute 'syntax region textSnip'.ft.'
  \ matchgroup='.a:textSnipHl.'
  \ start="'.a:start.'" end="'.a:end.'"
  \ contains=@'.group
endfunction

" == settings ==
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#show_buffers = 1
let g:airline#extensions#whitespace#enabled = 1
" let g:AutoPairs = {'(':')', '[':']', '{':'}',"'":"'",'"':'"', '`':'`', '「':'」', '“':'”'}
set background=dark
if hostname() =~# "linode"
    set notermguicolors
else
    set termguicolors
endif
colorscheme onedark
let g:airline_theme='onedark'

set autochdir " cd to file path (like emacs evil)

set noshowmode "mode is shown in statusline already, hide it
set number
set relativenumber
set path+=** " recursive completion
set wrap
set textwidth=0 " don't auto hard wrap by default please
set formatoptions+=mM

" Mirror my emacs setup
set clipboard+=unnamedplus
set ignorecase
set smartcase

"Tab
set tabstop=8
set softtabstop=0
set expandtab " ^T->space
set shiftwidth=4

set list
set listchars=eol:¬,tab:>-,nbsp:⎵

"Folding
set foldenable
set foldlevelstart=10
set foldnestmax=10
nnoremap <tab> za
set foldmethod=indent

"Wrap
set whichwrap=b,s,<,>,[,]

"Mouse support
set mouse=a

" == keys ==
inoremap jk <Esc>
inoremap kj <Esc>
"Space -> leader
nnoremap <space> <nop>
let mapleader = " "
inoremap <C-space> <ESC>
vnoremap <C-space> <ESC>

"leader based stuff
nnoremap <leader>c :noh<CR>
nnoremap <leader>eb :so %<CR>
nnoremap <leader>u :UndotreeToggle<CR>
nnoremap <leader>fs :w<CR>
nnoremap <leader>fn :new<CR>
nnoremap <leader>fq :wq<CR>
nnoremap <leader>ff :Files<CR>
nnoremap <leader>w <C-w>
nnoremap <leader>h :help
nnoremap <leader>gs :Magit<CR>

"Switch buffers
nnoremap <leader>bn :bn<CR>
nnoremap <leader>bp :bp<CR>
nnoremap <leader>bd :bd<CR>
nnoremap <leader>bb :Buffers<CR>

"Terminal
tnoremap <Esc><Esc> <C-\><C-n>

" highlight >120 char lines & trailing whitespaces
match ErrorMsg '\%>120v.\+'
match ErrorMsg '\s\+$'

" == LSP ==
lua <<EOF
local lspconfig = require 'lspconfig'
lspconfig.ts_ls.setup{}
EOF

" == augroups ==
augroup main
    autocmd!
    autocmd VimEnter * highlight clear SignColumn
augroup END

augroup web
    autocmd!
    autocmd FileType html call TextEnableCodeSnip('css', '<style>', '</style>', 'SpecialComment')
    autocmd FileType html setlocal equalprg=tidy\ -quiet\ --show-errors\ 0
augroup END

augroup java
    autocmd!
    autocmd FileType java setlocal noexpandtab
    autocmd FileType java setlocal list
    autocmd FileType java setlocal listchars=tab:+\ ,eol:-
    autocmd FileType java setlocal formatprg=par\ -w80\ -T4
augroup END

augroup php
    autocmd!
    autocmd FileType php setlocal expandtab
    autocmd FileType php setlocal list
    autocmd FileType php setlocal listchars=tab:+\ ,eol:-
    autocmd FileType php setlocal formatprg=par\ -w80\ -T4
augroup END

augroup fish
    autocmd!
    autocmd FileType fish setlocal tabstop=4
    autocmd FileType fish setlocal softtabstop=4
    autocmd FileType fish setlocal textwidth=79
    autocmd FileType fish setlocal foldmethod=expr
    autocmd FileType fish compiler fish
augroup END

augroup pollen
    autocmd!
    autocmd FileType pollen setlocal commentstring=◊;\ %s

    " from https://github.com/otherjoel/vim-pollen#use
    au! BufRead,BufNewFile *.pm set filetype=pollen
    au! BufRead,BufNewFile *.pp set filetype=pollen
    au! BufRead,BufNewFile *.ptree set filetype=pollen

    autocmd FileType pollen setlocal wrap      " Soft wrap (don't affect buffer)
    autocmd FileType pollen setlocal linebreak " Wrap on word-breaks only
augroup END

augroup ruby
    autocmd!
    autocmd FileType ruby setlocal tabstop=2
    autocmd FileType ruby setlocal shiftwidth=2
    autocmd FileType ruby setlocal softtabstop=2
    autocmd FileType ruby setlocal commentstring=#\ %s
augroup END

augroup python
    autocmd!
    autocmd FileType python setlocal commentstring=#\ %s
    autocmd BufEnter *.cls setlocal filetype=java
augroup END

augroup racket
    autocmd!
    autocmd FileType racket setlocal commentstring=;;\ %s
augroup END

augroup config
    autocmd!
    autocmd BufEnter *.zsh-theme setlocal filetype=zsh
    autocmd BufEnter custom_phrases.txt setlocal noexpandtab
    autocmd BufEnter Makefile setlocal noexpandtab
augroup END

augroup markdown
    autocmd!
    autocmd BufEnter markdown setlocal tabstop=2
    autocmd BufEnter markdown setlocal shiftwidth=2
    autocmd BufEnter markdown setlocal softtabstop=2
augroup END

augroup shell
    autocmd!
    " https://stackoverflow.com/questions/6366049/vim-in-file-commands
    autocmd FileType sh call TextEnableCodeSnip('racket', '<<RKT', 'RKT', 'SpecialComment')
    autocmd FileType sh call TextEnableCodeSnip('racket', '<<''RKT''', 'RKT', 'SpecialComment')
    autocmd FileType sh call TextEnableCodeSnip('c', '<<C', 'C', 'SpecialComment')
    autocmd FileType sh call TextEnableCodeSnip('c', '<<''C''', 'C', 'SpecialComment')
    autocmd FileType sh call TextEnableCodeSnip('javascript', '<<JS', 'JS', 'SpecialComment')
    autocmd FileType sh call TextEnableCodeSnip('javascript', '<<''JS''', 'JS', 'SpecialComment')
    autocmd FileType sh call TextEnableCodeSnip('json', '<<JSON', 'JSON', 'SpecialComment')
    autocmd FileType sh call TextEnableCodeSnip('json', '<<''JSON''', 'JSON', 'SpecialComment')
    autocmd FileType sh call TextEnableCodeSnip('python', '<<PYTHON', 'PYTHON', 'SpecialComment')
    autocmd FileType sh call TextEnableCodeSnip('python', '<<''PYTHON''', 'PYTHON', 'SpecialComment')
    autocmd BufEnter sh setlocal tabstop=2
    autocmd BufEnter sh setlocal shiftwidth=2
    autocmd BufEnter sh setlocal softtabstop=2
augroup END

augroup org
    autocmd!
    autocmd FileType org call TextEnableCodeSnip('sh', '#+BEGIN_SRC sh', '#+END_SRC', 'SpecialComment')
    autocmd FileType org call TextEnableCodeSnip('racket', '#+BEGIN_SRC racket', '#+END_SRC', 'SpecialComment')
    autocmd FileType org call TextEnableCodeSnip('clojure', '#+BEGIN_SRC clojure', '#+END_SRC', 'SpecialComment')
    autocmd FileType org call TextEnableCodeSnip('javascript', '#+BEGIN_SRC javascript', '#+END_SRC', 'SpecialComment')
    autocmd FileType org setlocal textwidth=0
augroup END

augroup javascript
    autocmd!
    autocmd FileType html call TextEnableCodeSnip('javascript', '<script>', '</script>', 'SpecialComment')
    autocmd BufEnter javascript setlocal tabstop=2
    autocmd BufEnter javascript setlocal shiftwidth=2
    autocmd BufEnter javascript setlocal softtabstop=2
augroup END

augroup vimscript
    autocmd!
    autocmd FileType vim call TextEnableCodeSnip('lua', 'lua << EOF', 'EOF', 'SpecialComment')
augroup END

augroup xml
    autocmd!
    autocmd FileType xml setlocal foldmethod=indent foldlevelstart=999 foldminlines=0
    autocmd FileType xml setlocal equalprg=tidy\ -quiet\ --show-errors\ 0
augroup END

augroup qml
    autocmd!
    autocmd FileType qml setlocal shiftwidth=2
augroup END

