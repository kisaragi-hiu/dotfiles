#!/bin/bash

# Line-wise comment hack from https://stackoverflow.com/a/9522766/6927814
args=(-mindepth 1 -maxdepth 1                # List immediate children but not itself
    -not -name thumbnails                    # Exclude thumbnails directory
    -exec rm -r --interactive=never '{}' ";" # Delete them
)

# find "$HOME/.cache" "${args[@]}"
