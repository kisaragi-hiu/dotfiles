#!/bin/bash
if command -v mpv >/dev/null 2>/dev/null && find "$HOME"/.local/share/splash.* ; then
    mpv --no-border \
        --no-osd-bar \
        --no-osc \
        --no-window-dragging \
        --on-all-workspaces \
        --ontop \
        --fullscreen \
        --title=splash \
        "$(find "$HOME"/.local/share -maxdepth 1 -name 'splash.*' | head -n 1)"
    xsetroot -solid "#000000"
fi
