#!/bin/sh
":"; exec emacs --quick --script "$0" "$@" # -*-mode: emacs-lisp; lexical-binding: t-*-

(require 'dbus)
(require 'subr-x)

(setq debug-on-error t)
(defvar s:verbose nil)
(defvar s:previous nil
  "Previous mute status.
nil, `muted', or `unmuted'.
Used to keep track of mute status without asking system.")

(dolist (arg argv)
  (when (equal "--verbose" arg)
    (setq s:verbose t))
  (when (equal "--help" arg)
    (message (string-trim "
%s [options]

options:
  --verbose: print a message when doing something
  --help: print this message
")
             (file-name-base
              (or buffer-file-name load-file-name)))
    (when noninteractive
      (kill-emacs))))

(setq argv nil)

(defun s:prop (prop)
  "Get the property equivalent to this qdbus call:

qdbus org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 \
      org.mpris.MediaPlayer2.Player.PROP"
  (dbus-get-property
   :session
   "org.mpris.MediaPlayer2.spotify"
   "/org/mpris/MediaPlayer2"
   "org.mpris.MediaPlayer2.Player"
   prop))

(defun s:current-playing-status ()
  (s:prop "PlaybackStatus"))

(defun s:current-playing-title ()
  (caadr (assoc "xesam:title" (s:prop "Metadata"))))

(defun s:sinks ()
  "Return all PulseAudio sinks."
  (split-string
   (string-trim-right
    (shell-command-to-string "pactl list sinks short | cut -s -f 1"))
   "\n"))

(cl-defun s:system-set-mute (new-status)
  "Set system mute status to NEW-STATUS.
Mute system if NEW-STATUS is `muted', unmute if NEW-STATUS is `unmuted'.
Do nothing if NEW-STATUS is the same as `s:previous'."
  (when (eq new-status s:previous)
    (cl-return-from s:system-set-mute))
  (let ((value (pcase new-status
                 (`muted "1")
                 (`unmuted "0")
                 (_ (error "NEW-STATUS can only be `muted' or `unmuted'")))))
    (when s:verbose
      (message
       (pcase new-status
         (`muted "muting...")
         (`unmuted "unmuting..."))))
    (dolist (sink (s:sinks))
      (start-process "pactl" nil
                     "pactl" "set-sink-mute" sink value))
    (setq s:previous new-status)))

(defun s:tick ()
  (if (and (equal "Playing" (s:current-playing-status))
           (or (member (s:current-playing-title)
                       '("Advertisement" ""))))
      (s:system-set-mute 'muted)
    (s:system-set-mute 'unmuted)))

(defun s:main ()
  (let ((s:previous nil))
    (while t
      (sleep-for 3)
      (s:tick))))

(s:main)

;; vim: ft=lisp
