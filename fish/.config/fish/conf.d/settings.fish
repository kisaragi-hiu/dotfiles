# alias clear 'clear; fish_greeting'
alias diff 'diff --color=auto'
alias gk 'git push'
alias gj 'git pull'
alias gc 'git commit'
alias gd 'git diff'
alias gco 'git checkout'
alias gst 'git status'
alias gl 'git log'
alias vim 'echo using nvim...; sleep 0.1; nvim'
alias ls 'ls -a --color=auto'

if test -z "$TERMUX_ROOT"
    # Aliases that shouldn't be in Termux
    alias frog "raco frog"
    alias paste 'xclip -o -sel clip'
    alias pollen "raco pollen"
    alias racket-slideshow "/usr/bin/slideshow"
    alias screenshot "import -window root"
else
    # Termux specific aliases
    alias paste 'termux-clipboard-get'
end

if test (hostname) = MF-PC
    alias utau "env WINEPREFIX=$HOME/.wineprefix/UTAU wine 'C:/Program Files (x86)/UTAU/utau.exe'"
else if test (hostname) = MF-manjaro
    alias utau "env WINEPREFIX=$HOME/.wineprefix/UTAU wine 'C:/Program Files/UTAU/utau.exe'"
end

if type -q rlwrap
    alias sbcl 'rlwrap sbcl'
    alias rash-repl 'rlwrap rash-repl'
    # alias pil 'rlwrap pil'
end

set -x EDITOR nvim
set -x VISUAL nvim

bind \cg 'toggle_greeting; and echo \nToggled greeting message.; commandline -f repaint'
bind \cl "commandline -C 0; commandline -i 'launch '"
bind \ej 'echo; jobs; commandline -f repaint'
bind \eg 'echo; git status; commandline -f repaint'
bind \ec 'echo; git commit; commandline -f repaint'

set D /run/media/"$USER"/Data
# set C /run/media/"$USER"/Windows
set G "$HOME"/ドキュメント
set M /run/media/"$USER"/Data/mega
set P /run/media/"$USER"/Data/mega/Projects
set DIARY_DIR "$USER"/git/notes/diary
set XDG_DESKTOP_DIR "$HOME"/デスクトップ
set XDG_DOWNLOAD_DIR "$HOME"/ダウンロード
set XDG_TEMPLATES_DIR "$HOME"/テンプレート
set XDG_PUBLICSHARE_DIR "$HOME"/公開
set XDG_DOCUMENTS_DIR "$HOME"/ドキュメント
set XDG_MUSIC_DIR "$HOME"/音楽
set XDG_PICTURES_DIR "$HOME"/画像
set XDG_VIDEOS_DIR "$HOME"/ビデオ
set DOTFILES_DIR "$HOME"/.dotfiles/

set -x PATH "$HOME"/git/Sudocabulary "$HOME"/bin "$HOME"/.local/bin $PATH
if type -q npm
    set -x PATH "$HOME"/.local/share/npm-global/bin $PATH
end
if type -q racket
    set racket_version (racket --eval "(display (version))")
    set -x PATH "$HOME"/.racket/"$racket_version"/bin $PATH
end
if type -q gem
    set ruby_version \
    (ruby --version | cut -d p -f 1 | cut -d " " -f 2)
    set -x PATH "$HOME"/.gem/ruby/"$ruby_version"/bin $PATH
end
