function select-pulse-sink-inputs
    # This ideally should be replaced with a multi input dmenu/rofi/fzf etc.
    # helper, but this will do for now.
    # (get-pulse-sink-input-ids) is implemented in emacs. eval it in a running
    # emacs instance.
    emacsclient --eval '(make-symbol (get-pulse-sink-input-ids))' | sed 's/\\//g' | rofi -dmenu -p "mute" | sed 's/: .*//g'
end
