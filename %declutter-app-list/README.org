* Declutter App List

=lsp-plugins= and =x42-plugins= install a =.desktop= launcher shortcut for /every single plugin they provide/, rendering the Multimedia category in the desktop menu completely useless.

I'm pretty sure basically nobody uses these plugins standalone. Those who do likely knows how to start them from the command line either. Filling up the user's app list with a million top level shortcuts is just horrible.

This is a workaround for that: remove them after they're installed.

Install:

#+begin_src bash
./install
#+end_src
